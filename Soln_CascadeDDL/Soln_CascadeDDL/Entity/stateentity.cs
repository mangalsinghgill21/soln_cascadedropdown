﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soln_CascadeDDL.Entity
{
    public class stateentity
    {
        public dynamic stateid { get; set; }
        public dynamic statename { get; set; }
        public dynamic cityentity { get; set; }
    }
}