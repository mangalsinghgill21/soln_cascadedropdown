﻿CREATE TABLE [dbo].[tblhobby] (
    [Hobbyid]    NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [Hobbyname]  NVARCHAR (MAX) NOT NULL,
    [Interested] BIT            NOT NULL,
    [PersonId]   NUMERIC (18)   NOT NULL,
    CONSTRAINT [PK_tblhobby] PRIMARY KEY CLUSTERED ([Hobbyid] ASC)
);

