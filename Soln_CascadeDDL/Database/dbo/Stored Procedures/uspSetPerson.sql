﻿CREATE PROCEDURE [dbo].[uspSetPerson]
	@Command Varchar(MAX)=NULL,
	@PersonId Numeric(18,0)=NULL,
	@FirstName Varchar(50)=NULL,
	@LastName varchar(50)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)=NULL OUT,
	@GetUserId Numeric(18,0) OUT
AS
	
		BEGIN
		
		DECLARE @ErrorMessage Varchar(MAX)=NULL

		IF @Command='Add'
			BEGIN
				
				BEGIN TRANSACTION

				BEGIN TRY
					
					INSERT INTO tblperson
					(
						
						Firstname,
						Lastname
					)
					VALUES
					(
						@FirstName,
						@LastName
					)

					SELECT @GetUserId =@@IDENTITY

					SET @Status=1
					SET @Message='Inserted'

					 

					COMMIT TRANSACTION

				END TRY

				BEGIN CATCH
					
					SET @ErrorMessage=ERROR_MESSAGE()
					SET @Status=0
					SET @Message='failed'

					RAISERROR(@ErrorMessage,16,1)
					
					ROLLBACK TRANSACTION

				END CATCH 

			END 
			ELSE IF @Command='Update'
			BEGIN 
						
					BEGIN TRANSACTION

					BEGIN TRY 

					SELECT  p.FirstName, p.LastName
						FROM tblperson AS p
							WHERE p.PersonId=@PersonId

							UPDATE tblperson
								SET 
									Firstname=@FirstName,
									Lastname=@LastName
										WHERE PersonId=@PersonId

									SELECT @GetUserId =@@IDENTITY
										
								SET @Status=1
								SET @Message='Updated'

								COMMIT TRANSACTION

					END TRY 

					BEGIN CATCH 
						SET @ErrorMessage=ERROR_MESSAGE()
						ROLLBACK TRANSACTION
						
						SET @Status=0
						SET @Message='Update failed'

						RAISERROR(@ErrorMessage,16,1)
					END CATCH
					End

End